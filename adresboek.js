
let entryList;
if(localStorage.getItem('Entries') !== null){
    entryList = JSON.parse(localStorage.getItem('Entries'));
}else{
    entryList = [];
}

class Person {
    constructor (firstname,lastname,gender,phonenumber ="",birthdate = "" ,url){
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthdate = birthdate;
        this.gender = gender;
        this.phonenumber = phonenumber;
        this.imageURL = url;
    }
}

function addEntry (firstname,lastname,gender,phonenumber,birthdate) {
    let index = Math.floor((Math.random() * 100) + 1);
    let genderURL;
    switch(gender){
        case "woman": 
            genderURL = "women";
            break;
        case "man":
            genderURL = "men";
            break;
        default:
            genderURL = "lego";
            index = Math.floor((Math.random() * 6) + 1);
    }
    let link = "https://randomuser.me/api/portraits/"+ genderURL +"/" + index +".jpg";
     
    let person = new Person (firstname,lastname,gender,phonenumber,birthdate, link);
    entryList.push(person);
    localStorage.setItem('Entries',JSON.stringify(entryList));
    displayCards(entryList);
}

function sortByAge(a,b) {
    if(a.birthdate == ""){
        return 1;
    }
    if(b.birthdate == ""){
        return -1;
    }
    if(a.birthdate < b.birthdate){
        return -1;
    }
    if(a.birthdate > b.birthdate){
        return 1;
    }
    return 0;
}
function sortByFirstname (a,b){
    if(a.firstname < b.firstname){
        return -1;
    }
    if(a.firstname > b.firstname) {
        return 1;
    }
    return 0;
}
function sortByLastname(a,b) {
    if(a.lastname < b.lastname){
        return -1;
    }
    if(a.lastname > b.lastname) {
        return 1;
    }
    return 0;
}
function onSelect(e){
    let value = e.target.value;
    sort(value);
}

function filterName (currentValue, index, array) {
    let fullname = currentValue.firstname + " " + currentValue.lastname;
    let name = fullname.toLowerCase();
    let splitted = document.getElementById('inputfield').value.toLowerCase();
    return name.includes(splitted);
}


function sort(value){
    let sortfuction;
    switch(value){
        case "lastname":
            sortfuction = sortByLastname;
            break;
        case "firstname":
            sortfuction = sortByFirstname;
            break;
        case "age":
            sortfuction = sortByAge;
            break;
    }
    entryList.sort(sortfuction);
    displayCards(entryList);
}

function onAddClick() {
    var form = document.getElementById("form");
    var firstname = form.children[0].value;
    var lastname = form.children[1].value;
    var date = form.children[3].value;
    var phonenumber = form.children[2].value;
    var gender = document.getElementById("genderSelect").value;
    addEntry(firstname,lastname,gender,phonenumber,date);
    for(let i =0; i < 4; i++) {
        form.children[i].value = "";
    }
}

var addButton = document.getElementById("addButton");
addButton.addEventListener("click",onAddClick);

var select = document.getElementById("sortSelect");
sort(select.value);
select.addEventListener("change",onSelect);

var input = document.getElementById("inputfield");

input.addEventListener("keyup",function (){
    var newArray = entryList.filter(filterName);
    let container = document.getElementById("cardContainer");
    let innerHTML= "";
    for(p of newArray) {
        innerHTML += createCardElement(p);
    }
    container.innerHTML = innerHTML;
});

function createCardElement (entry) {
    return "<div class='col-md-4'><div class='card mb-4 box-shadow'><img class=' cardImg card-img-top' src='"+ entry.imageURL+"' alt='Card image cap'><div class='card-body'><p class='card-text'>"+ entry.firstname + " " + entry.lastname+"</p><p> Telefoonnummer: "+ entry.phonenumber+"</p><p>Geboortedatum: "+ entry.birthdate+"</p><div class='d-flex justify-content-between align-items-center'></div></div></div></div>";
}

function displayCards (entryArray){
    let innerHTML ='';
    for(card of entryArray){
        innerHTML += createCardElement(card);
    }
    let container = document.getElementById("cardContainer");
    container.innerHTML = innerHTML;
}
displayCards(entryList);